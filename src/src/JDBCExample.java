import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.apache.commons.lang3.ObjectUtils;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;

/**
 * Different types of JDBC usage
 */
public class JDBCExample {
    Connection conn; // DB connection

    /**
     * Empty constructor
     */
    public JDBCExample() {
        this.conn = null;
    }

    /**
     *
     * @return true if the connection was successfully set
     */
    public boolean openConnection() {

        System.out.print("Trying to connect... ");

        // creating the connection. Parameters should be taken from config file.
        String host = "localhost";
        String port = "3306";
        String schema = "app_db";
        String user = "root";
        String password = "123456";
        String timeZone ="?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

        try {
            conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + schema + timeZone, user, password);
        } catch (SQLException e) {
            System.out.println("Unable to connect - " + e.getMessage());
            conn = null;
            return false;
        }
        System.out.println("Connected!");
        return true;
    }

    /**
     * close the connection
     */
    public void closeConnection() {
        // closing the connection
        try {
            conn.close();
        } catch (SQLException e) {
            System.out.println("Unable to close the connection - " + e.getMessage());
        }

    }

    /**
     * Shows how to retrieve the generated ID (by "auto incremental")
     */
    // -------------------------------- load artists function --------------------------------
    public void loadArtists() throws IOException, CsvValidationException {
        //CSVReader reader = new CSVReader(new FileReader("src/100K.csv"));
        CSVReader reader = new CSVReader(new FileReader("src/artists.csv"));
//        String [] nextLine;
//        while ((nextLine = reader.readNext()) != null) {
//            // nextLine[] is an array of values from the line
//            System.out.println(nextLine[0]);
//        }
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();

                String sql = "Insert into artists values(?,?,?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    int counter = 1;
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[2]);
                            if (nextLine[12].equals("N")|| nextLine[12].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[12]);
                            if (nextLine[4].equals("N") || nextLine[4].equals("") || Integer.parseInt(nextLine[4])<1901)
                                ps.setString(4, null);
                            else
                                ps.setString(4,nextLine[4]);
                            System.out.println(nextLine[4]);
                            if (nextLine[9].equals("N")|| nextLine[9].equals(""))
                                ps.setString(5, null);
                            else
                                ps.setString(5,nextLine[9]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
                    else {
                        safelyClose(rs, stmt);
                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load instruments function --------------------------------
    public void loadInstruments() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("src/instrument.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();

                String sql = "Insert into instruments values(?,?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        //System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[2]);
                            if (nextLine[6].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[6]);
                            if (nextLine[7].equals(""))
                                ps.setString(4, null);
                            else
                                ps.setString(4,nextLine[7]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
                    else {
                        safelyClose(rs, stmt);
                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load places function --------------------------------
    public void loadPlaces() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/places.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();
                String sql = "Insert into places values(?,?,?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    int counter = 1;
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[2]);
                            if (nextLine[3].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[3]);
                            if (nextLine[10].equals("N") || nextLine[10].equals("") || Integer.parseInt(nextLine[10])
                                    <1901)
                                ps.setString(4, null);
                            else
                                ps.setString(4,nextLine[10]);
                            if (nextLine[13].equals("N") || nextLine[13].equals(""))
                                ps.setString(5, null);
                            else
                                ps.setString(5,nextLine[13]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
                    else {
                        safelyClose(rs, stmt);
                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }


    // -------------------------------- load area_type function --------------------------------
    public void loadAreaType() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/area_type.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();
                String sql = "Insert into area_type values(?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 8; i++){
                        //System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[1]);
                            ps.setString(3,nextLine[4]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
                    else {
                        safelyClose(rs, stmt);
                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load area function --------------------------------
    public void loadArea() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/area.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();
                String sql = "Insert into area values(?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        //System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[2]);
                            if (nextLine[3].equals("N") || nextLine[3].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[3]);
                            //ps.setString(3,nextLine[3]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load PlaceType function --------------------------------
    public void loadPlaceType() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/place_type.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();
                String sql = "Insert into place_type values(?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        //System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[1]);
                            if (nextLine[4].equals("N") || nextLine[4].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[4]);
                            //ps.setString(3,nextLine[3]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }


    // -------------------------------- load Place again (with FK) --------------------------------
    public void loadPlacesNew() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/places.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();
                String sql = "Insert into places values(?,?,?,?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        if ((nextLine = reader.readNext()) != null){
                            //System.out.print("line: " + i + "\t");
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[2]);
                            if (nextLine[3].equals("N") || nextLine[3].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[3]);
                            if (nextLine[10].equals("N") || nextLine[10].equals("") || Integer.parseInt(nextLine[10])
                                    <1901)
                                ps.setString(4, null);
                            else
                                ps.setString(4,nextLine[10]);
                            if (nextLine[13].equals("N") || nextLine[13].equals("")|| Integer.parseInt(nextLine[13])
                                    <1901)
                                ps.setString(5, null);
                            else
                                ps.setString(5,nextLine[13]);
                            if (nextLine[5].equals("N") || nextLine[5].equals(""))
                                ps.setString(6, null);
                            else
                                ps.setString(6,nextLine[5]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load language --------------------------------
    public void loadLanguages() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/language.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();
                String sql = "Insert into languages values(?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        if ((nextLine = reader.readNext()) != null){
                            //System.out.print("line: " + i + "\t");
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[4]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load release --------------------------------
    public void loadRelease() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/release.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null && counter < 20) {
                stmt = conn.createStatement();
                String sql = "Insert into releases values(?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        if ((nextLine = reader.readNext()) != null){
                            //System.out.print("line: " + i + "\t");
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[2]);
                            if (nextLine[7].equals("N") || nextLine[7].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[7]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load work_type --------------------------------
    public void loadWorkType() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/work_type.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();
                String sql = "Insert into work_type values(?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        if ((nextLine = reader.readNext()) != null){
                            //System.out.print("line: " + i + "\t");
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[1]);
                            if (nextLine[4].equals("N") || nextLine[4].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[4]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load work --------------------------------
    public void loadWork() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/work.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null && counter < 20) {
                stmt = conn.createStatement();
                String sql = "Insert into works values(?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        if ((nextLine = reader.readNext()) != null){
                            //System.out.print("line: " + i + "\t");
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[2]);
                            if (nextLine[3].equals("N") || nextLine[3].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[3]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }


    // -------------------------------- load link_work_language --------------------------------
    public void loadLinkWorkLang() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/link_work_languages.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null && counter < 20) {
                stmt = conn.createStatement();
                String sql = "Insert into l_work_language values(?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        if ((nextLine = reader.readNext()) != null){
                            //System.out.print("line: " + i + "\t");
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[1]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load Recording function --------------------------------
    public void loadRecording() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/recording.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null && counter < 45) {
                stmt = conn.createStatement();
                String sql = "Insert into recordings values(?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        //System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[2]);
                            if (nextLine[3].equals("N") || nextLine[3].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[3]);
                            //ps.setString(3,nextLine[3]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }


    // -------------------------------- load ArtistType function --------------------------------
    public void loadArtistType() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/artist_type.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null && counter < 25) {
                stmt = conn.createStatement();
                String sql = "Insert into artist_type values(?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        //System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[1]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }


    // -------------------------------- load Gender function --------------------------------
    public void loadGender() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/gender.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null && counter < 25) {
                stmt = conn.createStatement();
                String sql = "Insert into gender values(?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        //System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[1]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }



    // -------------------------------- load artistsNew (with FK) function --------------------------------
    public void loadArtistsNew() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/artists.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            while ((nextLine = reader.readNext()) != null) {
                stmt = conn.createStatement();

                String sql = "Insert into artists1 values(?,?,?,?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        //System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,nextLine[0]);
                            ps.setString(2,nextLine[2]);
                            if (nextLine[12].equals("N")|| nextLine[12].equals(""))
                                ps.setString(3, null);
                            else
                                ps.setString(3,nextLine[12]);
                            if (nextLine[4].equals("N") || nextLine[4].equals("") || Integer.parseInt(nextLine[4])<1901)
                                ps.setString(4, "4");
                            else
                                ps.setString(4,nextLine[4]);
                            //System.out.println(nextLine[4]);
                            if (nextLine[9].equals("N")|| nextLine[9].equals(""))
                                ps.setString(5, null);
                            else
                                ps.setString(5,nextLine[9]);
                            if (nextLine[10].equals("N")|| nextLine[10].equals(""))
                                ps.setString(6, null);
                            else
                                ps.setString(6,nextLine[10]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    // -------------------------------- load linkRecordingRelese function --------------------------------
    public void loadLinkRecordingRelease() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReader(new FileReader("raw_data/link_recording_release.csv"));
        Statement stmt = null;
        ResultSet rs = null;
        int id;

        try {
            String [] nextLine;
            int counter = 1;
            int AI = 1;
            while ((nextLine = reader.readNext()) != null && counter < 25) {
                stmt = conn.createStatement();
                String sql = "Insert into l_recording_release values(?,?,?)";
                PreparedStatement preparedStatement = null;
                PreparedStatement ps=null;
                try{
                    //preparedStatement = conn.prepareStatement(sql);
                    ps=conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    System.out.println("batch:" + counter);
                    for (int i=0; i < 2000; i++){
                        //System.out.print("line: " + i + "\t");
                        if ((nextLine = reader.readNext()) != null){
                            ps.setString(1,String.valueOf(AI));
                            AI += 1;
                            ps.setString(2,nextLine[2]);
                            ps.setString(3,nextLine[3]);
                            ps.addBatch();
                        }
                    }
                    counter++;
                    int[] affectedRecords = ps.executeBatch();

                    rs = ps.getGeneratedKeys();
                    if (rs.next()){
                        id = rs.getInt(1);
                        System.out.println("Success - GetGeneratedID, the generated ID is: " + id);
                    }
//                    else {
//                        safelyClose(rs, stmt);
//                    }
                }finally {
                }
            }
        } catch (SQLException e) {
            System.out.println("ERROR executeUpdate - " + e.getMessage());
        } finally {
            safelyClose(rs, stmt);
        }
    }

    /**
     * Attempts to close all the given resources, ignoring errors
     *
     * @param resources
     */
    private void safelyClose(AutoCloseable... resources) {
        for (AutoCloseable resource : resources) {
            try {
                resource.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Prints the time difference from now to the input time.
     */
    private void printTimeDiff(long time) {
        time = (System.currentTimeMillis() - time) / 1000;
        System.out.println("Took " + time + " seconds");
    }

}
