import com.opencsv.exceptions.CsvValidationException;

import java.io.IOException;

/**
 * JDBC usage examples
 */
public class MainJDBC {

    public static void main(String[] args) throws IOException, CsvValidationException {
        JDBCExample jdbcExample;

        // creating the example object
        jdbcExample = new JDBCExample();

        // connecting
        if (!jdbcExample.openConnection())
            return;


//        jdbcExample.loadAreaType();
        jdbcExample.loadArea();
        //jdbcExample.loadArtistType();
        //jdbcExample.loadGender();
        //jdbcExample.loadArtistsNew();
        //jdbcExample.loadInstruments();
        //jdbcExample.loadLanguages();
        //jdbcExample.loadRecording();
        //jdbcExample.loadLinkRecordingRelease();
        //jdbcExample.loadPlaceType();
        //jdbcExample.loadPlacesNew();
//        jdbcExample.loadRelease();
        //jdbcExample.loadWorkType();
        //jdbcExample.loadWork();
        //jdbcExample.loadLinkWorkLang();

        // Don't run these!
        ////jdbcExample.loadArtists();
        ////jdbcExample.loadPlaces();


        // close the connection
        jdbcExample.closeConnection();

        System.out.println("Done :)");
    }

}
