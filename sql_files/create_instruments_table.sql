CREATE TABLE `app`.`instruments` (
      `id_instrument` INT(11) NOT NULL,
      `instrument_name` VARCHAR(39) NOT NULL,
      `short_explanation` VARCHAR(185) NULL DEFAULT NULL,
      `long_explanation` VARCHAR(895) NULL DEFAULT NULL,
      PRIMARY KEY (`id_instrument`, `instrument_name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

