CREATE TABLE `app`.`places` (
      `id_places` INT(11) NOT NULL,
      `place_name` VARCHAR(90) NOT NULL,
      `place_type` ENUM('studio', 'venue', 'other', 'stadium', 'indoor arena', 'religious building', 'educational institution', 'pressing plant') NULL DEFAULT NULL,
      `founded_date` YEAR(4) NULL DEFAULT NULL,
      `closed_date` YEAR(4) NULL DEFAULT NULL,
      PRIMARY KEY (`id_places`, `place_name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

