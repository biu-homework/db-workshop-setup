CREATE TABLE `app`.`artists` (
      `id_artist` INT(11) NOT NULL,
      `artist_name` VARCHAR(100) NOT NULL,
      `gender` ENUM('male', 'female', 'Other', 'Not applicable') NULL DEFAULT NULL,
      `birth_date` YEAR(4) NULL DEFAULT NULL,
      `death_date` YEAR(4) NULL DEFAULT NULL,
      PRIMARY KEY (`id_artist`, `artist_name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
